CREATE EXTENSION pgcrypto;

CREATE TABLE roles (
  id SERIAL PRIMARY KEY,
  role varchar(30) NOT NULL UNIQUE
);


CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  user_name varchar(50) NOT NULL UNIQUE,
  password varchar(100) NOT NULL,
  role_id integer NOT NULL,
  CONSTRAINT fk_role
        FOREIGN KEY(role_id)
    REFERENCES roles(id)
);

INSERT INTO roles (role) VALUES ('admin'), ('guest');

INSERT INTO users (user_name, password, role_id) VALUES (
  'mvijayaragavan@live.com',
  '$2b$10$n/iAtXTaFGiWgxIypnaII.teBjBXuRJIG/0PBWtGc8gtIyjhzxt6C',
  1
);