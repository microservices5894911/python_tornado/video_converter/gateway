#!/usr/bin/env sh

set -ex

test -n "$SHELLDEBUG" && set -x

TEST_HOST="${TEST_HOST:-127.0.0.1}"

get_exposed_port() {
  docker-compose port "$@" | cut -d: -f2
}

wait_for() {
  service=$1
  shift
  printf 'Waiting for %s... ' $service
  if ! wait-for -t ${WAIT_TIMEOUT:-30} "$@"
  then
      echo 'FAILED!'
      return 1
  fi
  echo 'done.'
}
rm -rf build
mkdir build

docker-compose pull
docker-compose down --timeout 0 --remove-orphans --volumes

docker-compose up -d appdb
wait_for appdb postgresql://postgres:pgpassword@$TEST_HOST:$(get_exposed_port appdb 5432)/app

docker-compose up -d auth
wait_for auth http://$TEST_HOST:$(get_exposed_port auth 8000)/status

PGSQL_APP=postgresql://postgres:pgpassword@$TEST_HOST:$(get_exposed_port appdb 5432)/app \
  prep-it

echo Environment variables:
tee build/test-environment << EOF
export AUTH_URL=http://${TEST_HOST}:$(get_exposed_port auth 8000)
export PGSQL_APP=postgresql://postgres:pgpassword@${TEST_HOST}:$(get_exposed_port appdb 5432)/app
EOF

. ./build/test-environment

echo Bootstrap complete
