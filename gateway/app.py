import os

import aiopg
import pkg_resources
import sprockets.http
import sprockets.http.app
from sprockets.mixins.mediatype import content, transcoders
from tornado import web

from gateway import handlers, version


class Application(sprockets.http.app.Application):
    def __init__(self, **kwargs):
        urls = [
            web.url('/', handlers.OpenApiHandler),
            web.url(r'/(openapi.yaml)', handlers.OpenApiHandler),
            web.url(r'/status', handlers.StatusHandler),
            web.url(r'/login', handlers.LoginHandler),
            web.url(r'/validate_token', handlers.TokenValidateHandler)
        ]
        service = os.environ.get('SERVICE', 'gateway')
        kwargs.update({'service': service, 'version': version})
        super().__init__(urls,
                         static_path=pkg_resources.resource_filename(
                             __package__, 'static'),
                         template_path=pkg_resources.resource_filename(
                             __package__, 'templates'),
                         **kwargs)

        environment = os.environ.get('ENVIRONMENT', 'development')
        self.settings.update({
            'aiopg_execute_timeout': int(
                os.environ.get('AIOPG_EXECUTE_TIMEOUT', 60)),
            'environment': environment,
            'services': {
                'auth': os.environ['AUTH_URL']
            }
        })

        self.settings['static_path'] = os.environ.get(
            'STATIC_PATH', pkg_resources.resource_filename(__name__, 'static'))
        self.settings['template_path'] = pkg_resources.resource_filename(
            __name__, 'templates')

        content.set_default_content_type(self, 'application/json', 'utf-8')

        json = transcoders.JSONTranscoder()
        json.dump_options['sort_keys'] = True
        content.add_transcoder(self, json)

        self.ready_to_serve = False
        self.on_start_callbacks.append(self.on_start)
        self.on_shutdown_callbacks.append(self.on_shutdown)

    async def on_start(self, *_):
        self.appdb = await aiopg.create_pool(os.environ['PGSQL_APP'])
        self.ready_to_serve = True

    async def on_shutdown(self, _):
        self.appdb.close()
        await self.appdb.wait_closed()


def run():
    sprockets.http.run(Application,
                       settings={
                           'number_of_procs': 1,
                           'xheaders': True
                       })
