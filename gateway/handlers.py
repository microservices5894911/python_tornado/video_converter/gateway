import pkg_resources
import sprockets.mixins.http
import yarl

from gateway import helpers, version


class GatewayHandler(sprockets.mixins.http.HTTPClientMixin,
                     helpers.RequestHandler):
    def initialize(self):
        super().initialize()


class OpenApiHandler(helpers.RequestHandler):
    """Services up the index page and OpenAPI doc"""
    def get(self, *args, **kwargs):
        template = args[0] if args else "index.html"
        if args and args[0] == "openapi.yaml":
            self.set_header("Content-Type", "text/yaml")
        self.set_header("Access-Control-Allow-Origin", "*")
        self.render(
            template,
            **{
                "host": self.request.host,
                "scheme": self.request.protocol,
                "settings": self.settings,
                "version": pkg_resources.get_distribution(__package__).version,
                "name": "Gateway Service",
            },
        )


class StatusHandler(helpers.RequestHandler):
    """
    Simple heartbeat handler.

    Doesn't check anything beyond IOLoop responsiveness.

    """
    def get(self):
        if self.application.ready_to_serve:
            status = 200
            text = 'ok'
        else:
            status = 503
            text = 'maintenance'
        self.set_status(status)
        self.send_response({
            'service': self.settings['service'],
            'version': version,
            'status': text
        })


class LoginHandler(GatewayHandler):

    async def post(self):
        body = self.get_request_body()

        try:
            _ = body['user_name']
            _ = body['password']
        except KeyError:
            self.send_error(403, reason='Missing credentials')
            return

        url = yarl.URL(self.settings['services']['auth']).with_path('login')
        response = await self.http_fetch(
            url,
            method='POST',
            body=body,
            content_type='application/json')

        if response.code == 200:
            self.send_response({'token': response.body['token']})
        else:
            self.send_error(response.code, reason=response.body)


class TokenValidateHandler(GatewayHandler):
    async def get(self):
        if 'Authorization' not in self.request.headers:
            self.send_error(403, reason='Missing token')
            return

        url = yarl.URL(self.settings['services']['auth']).with_path(
            'validate_token')
        response = await self.http_fetch(
            url,
            request_headers=self.request.headers)

        if response.code == 200:
            self.send_response(response.body)
        else:
            self.send_error(response.code, reason=response.body)
