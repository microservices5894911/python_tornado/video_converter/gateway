import logging
import uuid

import sprockets.http.mixins
from sprockets.mixins.mediatype import content
from tornado import web


class RequestHandler(content.ContentMixin, sprockets.http.mixins.ErrorLogger,
                     sprockets.http.mixins.ErrorWriter, web.RequestHandler):
    """Basic request handler.

      This is the base class for all request handlers.  It contains
      functionality and behavior that is available everywhere.

    """
    access_log_failures_only = True

    def __init__(self, *args, **kwargs):
        # this needs to be set BEFORE super.__init__ since it WILL
        # BE referred to from within set_default_headers during
        # super().__init()
        self.correlation_id = str(uuid.uuid4())
        super().__init__(*args, **kwargs)
        self.logger = logging.getLogger(self.__class__.__name__)
