#!/usr/bin/env sh
# testing script for the CI pipeline
set -ex

pip install -e '.[tests]'

rm -rf build && mkdir -p build

echo 'Running flake8...'
if ! flake8 --output-file=build/flake8.txt
then
    echo 'ERROR: flake8 detected PEP8 violations'
    cat build/flake8.txt
    echo ''
    exit 1
fi

echo 'Running yapf...'
if ! yapf -dr auth tests >build/yapf.diff
then
    echo 'ERROR: yapf detected formatting violations'
    cat build/yapf.diff
    echo ''
    exit 1
fi

echo "Running bootstrap..."
./bootstrap.sh
. ./build/test-environment

echo "Running tests..."
coverage run
coverage report
coverage xml


#echo 'Building docker image...'
#rm -fr dist/*
#./setup.py sdist
#docker-compose up --build -d auth
#host=${TEST_HOST:-127.0.0.1}
#port=`docker-compose port auth 8000 | cut -d: -f2`
#wait-for "http://$host:$port/status"
