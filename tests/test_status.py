import json

from gateway import version
from tests import helpers


class StatusTests(helpers.TestCase):
    def test_status_ok(self):
        response = self.fetch('/status')
        self.assertEqual(response.code, 200)
        service = self.app.settings['service']
        expected = {
            'service': service,
            'status': 'ok',
            'version': version,
        }
        self.assertEqual(expected, json.loads(response.body.decode()))
        self.assertEqual(f'{service}/{version}', response.headers['Server'])

    def test_status_maintenance(self):
        self.app.ready_to_serve = False
        response = self.fetch('/status')
        self.assertEqual(response.code, 503)
        service = self.app.settings['service']
        expected = {
            'service': service,
            'status': 'maintenance',
            'version': version,
        }
        self.assertEqual(expected, json.loads(response.body.decode()))
        self.assertEqual(f'{service}/{version}', response.headers['Server'])
