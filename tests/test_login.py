import json

from tests import helpers


class LoginTests(helpers.TestCase):
    def setUp(self):
        super().setUp()
        self.headers = {'Content-Type': 'application/json'}
        self.body = {
            "user_name": "mvijayaragavan@live.com",
            "password": "test_vijay"
        }

    def test_login(self):
        response = self.fetch('/login',
                              method='POST',
                              body=json.dumps(self.body),
                              headers=self.headers)
        self.assertEqual(200, response.code)
        res = json.loads(response.body.decode('utf8'))
        self.assertIsNotNone(res['token'])

    def test_login_invalid_password(self):
        self.body['password'] = 'invlaid_password'
        response = self.fetch('/login',
                              method='POST',
                              body=json.dumps(self.body),
                              headers=self.headers)
        self.assertEqual(401, response.code)
        self.assertEqual(response.reason, 'Invalid credentials')

    def test_login_invalid_username(self):
        self.body['user_name'] = 'invlaid_username'
        response = self.fetch('/login',
                              method='POST',
                              body=json.dumps(self.body),
                              headers=self.headers)
        self.assertEqual(401, response.code)
        self.assertEqual(response.reason, 'Invalid credentials')

    def test_login_missing_credentials(self):
        del self.body['password']
        response = self.fetch('/login',
                              method='POST',
                              body=json.dumps(self.body),
                              headers=self.headers)
        self.assertEqual(403, response.code)
        self.assertEqual(response.reason, 'Missing credentials')


class ValidateTokenTests(helpers.TestCase):
    def setUp(self):
        super().setUp()
        self.headers = {'Content-Type': 'application/json'}
        self.body = {
            "user_name": "mvijayaragavan@live.com",
            "password": "test_vijay"
        }

    def test_validated_token(self):
        response = self.fetch('/login',
                              method='POST',
                              body=json.dumps(self.body),
                              headers=self.headers)
        self.assertEqual(200, response.code)
        res = json.loads(response.body.decode('utf8'))
        token = res['token']
        self.headers['Authorization'] = 'Bearer %s' % token

        response = self.fetch('/validate_token', headers=self.headers)
        res = json.loads(response.body.decode('utf8'))

        self.assertEqual(res['username'], self.body['user_name'])
        self.assertEqual(res['role'], 'admin')

    def test_validated_token_without_header(self):
        response = self.fetch('/validate_token', headers=self.headers)
        res = json.loads(response.body.decode('utf8'))
        self.assertEqual(res['message'], 'Missing token')

    def test_validated_token_without_token(self):
        self.headers['Authorization'] = 'Bearer '
        response = self.fetch('/validate_token', headers=self.headers)
        res = json.loads(response.body.decode('utf8'))
        self.assertEqual(res['message'], 'Missing token')

    def test_validated_token_with_invalid_token(self):
        self.headers['Authorization'] = 'Bearer eyJhbGciOiJIUzI1NiIsInR' \
                                        '5cCI6IkpXVCJ9.eyJ1c2VybmFtZS' \
                                        'I6Im12aWpheW'

        response = self.fetch('/validate_token', headers=self.headers)
        res = json.loads(response.body.decode('utf8'))
        self.assertEqual(res['message'], 'Invalid token')

    def test_validated_token_with_expired_token(self):
        self.headers['Authorization'] = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6' \
                                        'IkpXVCJ9.eyJ1c2VybmFtZSI6Im12aWpheW' \
                                        'FyYWdhdmFuQGxpdmUuY29tIiwiZXhwIjoxN' \
                                        'jc4ODgyNTc0LCJpYXQiOjE2Nzg4ODI1NDQs' \
                                        'ImFkbWluIjp0cnVlfQ.dlIFDpH4uZ3X2hNX' \
                                        'PIBAJyWbgWevgGZM-ejXBt9CeDQ'

        response = self.fetch('/validate_token', headers=self.headers)
        res = json.loads(response.body.decode('utf8'))
        self.assertEqual(res['message'], 'Token expired')
