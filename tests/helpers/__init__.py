import asyncio
import os

import sprockets.http.testing

from gateway import app


class ResetEnvironmentMixin:
    def setUp(self):
        super().setUp()
        self.addCleanup(self.restore_environment, os.environ.copy())

    @staticmethod
    def restore_environment(environment):
        os.environ.clear()
        os.environ.update(environment)


class TestCase(sprockets.http.testing.SprocketsHttpTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

    def setUp(self):
        super().setUp()
        while not self.app.ready_to_serve:
            self.io_loop.asyncio_loop.run_until_complete(asyncio.sleep(0.05))

    def tearDown(self):
        super().tearDown()

    def get_app(self):
        self.app = app.Application()
        return self.app
