from tests import helpers


class TestOpenAPIHandler(helpers.TestCase):
    def test_getting_yaml(self):
        response = self.fetch('/openapi.yaml')
        self.assertEqual(response.code, 200)
        self.assertIn('text/yaml', response.headers['Content-Type'])

    def test_getting_index(self):
        response = self.fetch('/')
        self.assertEqual(response.code, 200)
        self.assertIn('text/html', response.headers['Content-Type'])
