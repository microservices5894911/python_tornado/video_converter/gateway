============
Gateway Service
============

Quickstart Development Guide
============================

Setting up your environment
---------------------------
.. code-block:: bash

   $ python3.9 -m venv env
   $ . ./env/bin/activate
   $ pip install -e '.[docs,tests]'
   $ ./bootstrap.sh
   $ . ./build/test-environment
