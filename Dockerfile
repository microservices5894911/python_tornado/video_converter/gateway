FROM mvijayaragavan/python3-testing:3.9.16-0 AS service
LABEL url="https://gitlab.com/microservices5894911/python_tornado/video_converter/gateway"
ENV SERVICE=gateway
COPY dist/*.tar.gz /tmp/
EXPOSE 8000
RUN apk add --no-cache --virtual .build gcc linux-headers musl-dev \
 && pip install --no-cache-dir --requirement /tmp/installed-packages.txt \
 && pip install --no-cache-dir /tmp/*.tar.gz \
 && apk del --purge .build \
 && rm -fr /var/cache/apk/*
CMD "${SERVICE}"
